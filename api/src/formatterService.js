function formatResults (results, provideScore = false) {
  return results.body.hits.hits.map((item) => {
    const result = {
      title: item._source.title,
      comments: item._source.comments,
      duration: item._source.duration,
      views: item._source.views,
      tags: item._source.tags,
      ratings: item._source.ratings
    };

    if (provideScore) {
      result.score = item._score;
    }

    return result
  })
}

module.exports = { formatResults }

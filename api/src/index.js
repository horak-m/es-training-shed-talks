const express = require('express')
const cors = require('cors')
const { Client } = require('@elastic/elasticsearch')
const formatterService = require('./formatterService')

const app = express()
const PORT = 3000

const client = new Client({ node: 'http://es:9200' })
const INDEX_NAME = 'shed-talks'

////////////////////////////////////
// MODIFY HERE
////////////////////////////////////
/**
 * @param searchQuery String|null - searched query e.g. "fossil fuels"
 * @param tags Array|null - list of tags e.g. ['marketing', 'leadership'] - treated as AND
 * @param ratings Array|null - list of ratings e.g. [['Inspiring', 100], ['Informative', 150]] - treated as OR
 */
function createQuery (
  searchQuery,
  tags,
  ratings
) {
  // this will be wrapped in top level "query", so don't create that
  return {
    match_all: {}
  }
}
////////////////////////////////////

async function search (
  searchQuery,
  tags,
  ratings
) {
  return client.search({
    index: INDEX_NAME,
    body: {
      query: createQuery(searchQuery, tags, ratings)
    }
  })
}

function getWrappedQuery (searchQuery, tags, ratings) {
  return {
    query: createQuery(searchQuery, tags, ratings)
  }
}

app.use(cors({ origin: '*' }))

app.get('/', (req, res) => {
  let tags = null
  let ratings = null
  const searchQuery = req.query.q || null

  if (req.query.tags) {
    tags = req.query.tags.split(',')
  }

  if (req.query.ratings) {
    ratings = req.query.ratings.split(',').map((item) => [item.split(':')[0], Number.parseInt(item.split(':')[1])])
  }

  console.log('q: ', searchQuery, 'tags: ', tags, 'ratings: ', ratings)

  search(
    searchQuery,
    tags,
    ratings
  ).then((data) => {
    res.send({
      data: formatterService.formatResults(data),
      query: getWrappedQuery(searchQuery, tags, ratings)
    })
  }).catch((error) => {
    res.send({
      query: getWrappedQuery(searchQuery, tags, ratings),
      error
    })
  })
})

app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`)
})

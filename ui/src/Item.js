import React from 'react'

class Item extends React.Component {
  render () {
    const scoreSpan = this.props.item.score ? <span><span style={{ fontWeight: 'bold', color: "red" }}>Score:</span> {this.props.item.score}<br /></span> : null;

    return (
      <div style={{ margin: 15 }}>
        <span style={{ fontSize: 'large', textDecoration: 'underline' }}>{this.props.item.title}</span><br />
        {scoreSpan}
        <span style={{ fontWeight: 'bold' }}>Duration:</span> {this.props.item.duration}<br />
        <span style={{ fontWeight: 'bold' }}>Views:</span> {this.props.item.views}<br />
        <span style={{ fontWeight: 'bold' }}>Tags:</span> {this.props.item.tags.join(', ')}<br />
        <span style={{ fontWeight: 'bold' }}>Ratings:</span> {this.props.item.ratings.map(item => `${item.name}: ${item.count}`).join(', ')}
      </div>
    )
  }
}

export default Item

import React from 'react'
import './App.css'
import Header from './Header'
import Item from './Item'
import SearchForm from './Form'

const style = {
  backgroundColor: '#1f4662',
  color: '#fff',
  fontSize: '12px'
}

const headerStyle = {
  backgroundColor: '#193549',
  padding: '5px 10px',
  fontFamily: 'monospace',
  color: '#ffc600'
}

const preStyle = {
  display: 'block',
  padding: '10px 30px',
  margin: '0',
  overflow: 'scroll'
}

let queryDisplayed = false

class App extends React.Component {
  constructor (props) {
    super(props)
    this.state = { items: [], error: null, query: null }
  }

  render () {
    const searchCallback = query => {
      const url = new URL('http://localhost:3000')
      url.search = new URLSearchParams(query).toString()

      fetch(url).then(response => response.json()).then(
        data => {
          if (!data.data || !Array.isArray(data.data) || data.error) {
            this.setState({
              items: [],
              error: JSON.stringify(data.error, null, 2),
              query: JSON.stringify(data.query, null, 2)
            })

            return
          }

          const items = data.data.map(item => <Item item={item} />)
          this.setState({
            items: items,
            error: null,
            query: JSON.stringify(data.query, null, 2)
          })
        }
      )
    }

    const queryDiv = (
      <div
        style={style} onClick={() => {
          document.getElementById('query-div').style.display = queryDisplayed ? 'none' : 'block'
          queryDisplayed = !queryDisplayed
        }}
      >
        <div style={headerStyle}>
          <strong>Query</strong>
        </div>
        <pre style={{ ...preStyle, display: 'none' }} id='query-div'>
          <button onClick={(e) => {
            navigator.clipboard.writeText(this.state.query ?? '-')
            e.stopPropagation()
          }}
          >
            Copy query
          </button><br /><br />
          {this.state.query ?? '-'}
        </pre>
      </div>
    )

    const errorDiv = this.state.error
      ? (
        <div style={style}>
          <div style={headerStyle}>
            <strong>Pretty Debug</strong>
          </div>
          <pre style={preStyle}>
            {this.state.error}
          </pre>
        </div>
        )
      : null

    return (
      <div className='App'>
        <Header />
        <SearchForm searchCallback={searchCallback} />
        <div style={{ fontSize: 'large', borderTop: 'solid' }}>Number of results: {this.state.items.length}</div>
        <div style={{ fontSize: 'large', borderBottom: 'solid' }}>{(new Date()).toLocaleTimeString('cz-CS')}</div>
        <div style={{ textAlign: 'left' }}>
          {queryDiv}
          {this.state.items}
          {errorDiv}
        </div>
      </div>
    )
  }
}

export default App

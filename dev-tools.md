# First and foremost - docs

[Elasticsearch documentation](https://www.elastic.co/guide/en/elasticsearch/reference/7.12/documents-indices.html)

# Nodes, clusters, ..

## Get info about cluster health
```
GET /_cluster/health
```

## Get info about nodes
```
GET /_cat/nodes?v
```
- query param `v` serves as "verbose" - outputs headers

## Get info about indices
```
GET /_cat/indices?v
```

## Get info about shards
```
GET /_cat/shards?v
```

# Indices

## Create index
```
PUT /cars
```
- default number of `shards` is 1
- default number of `replicas` is 1

## Create index with shard settings
```
PUT /bikes
{
  "settings": {
    "number_of_shards": 5,
    "number_of_replicas": 0
  }
}
```

## Deletes index
```
DELETE /bikes
```

# Documents

## Index a document
```
POST /cars/_doc
{
  "manufacturer": "Mazda",
  "model": "Miata",
  "color": "blue"
}
```
- document ID is a string
- ID is generated
- mapping is implicit - ES determines it automatically, once document is indexed
    - if document contains new fields, then they are added to the mapping
    - old mappings cannot be changed

## Index a document with specific ID
```
PUT /cars/_doc/<ID>

PUT /cars/_doc/1
{
  "manufacturer": "Audi",
  "model": 5,
  "color": "orange"
}
```
- if document with given ID exists, then it's overwritten
- values are coerced by default - e.g. number converted to string 

## Get document by ID
```
GET /cars/_doc/1
```

## Update document
```
POST /cars/_update/1
{
  "doc": {
    "speed": "brrrrr"
  }
}
```
- if field is not presented, field is added
- if field is presented, then it's modified

## Scripted document update
```
POST /cars/_update/1
{
  "script": {
    "source": "ctx._source.model--"
  }
}
```
- scripting language called `Painless`

## Upsert
```
POST /cars/_update/2
{
  "script": {
    "source": "ctx._source.model--"
  },
  "upsert": {
    "manufacturer": "VW",
    "model": "Golf"
  }
}
```
- if document presented, script is executed
- if document not exists, is inserted from `upsert`

## Delete document
```
DELETE /cars/_doc/2
```

# Mapping
- defines document structure
- can be loosely translated "table scheme"
- explicit mapping = is defined manually
- specifying mapping for a field doesn't make it mandatory
- all fields can be used as arrays - having 0 or more values

## data types (not full list)
- object 
- boolean
- short
- long
- float
- keyword - used for exact matching
- text - used for fulltext searching
- date - default format ISO-8601
- IP
- object
- nested

## Get mapping
```
GET /cars/_mapping
```

## Explicit mapping 
```
PUT /movies
{
  "mappings": {
    "properties": {        # properties inderectly define "object" type
      "name": {
        "type": "text"
      },
      "tags": {
        "type": "keyword"
      },
      "rating": {
        "type": "float"
      },
      "rewievs": {
        "properties": {    # nested object
          "full_name": {
            "type": "text"
          },
          "review": {
            "type": "text"
          }
        }
      }
    }
  }
}
```
- fields are flattened for access - `reviews.full_name`

### Index document in the index with specified mapping
```
PUT /movies/_doc/1
{
  "name": "Sharknado",
  "tags": ["action", "comedy"],
  "rating": 1.1,
  "rewies": [
    {
      "full_name": "John Doe",
      "review": "It's awesome!"
    }
  ]
}
```

### Index document in the index with specified mapping and extra field
```
PUT /movies/_doc/2
{
  "name": "Vampires",
  "tags": "action",
  "rating": 1.1,
  "rewies": [
    {
      "full_name": "Hort",
      "review": "It's awesome!"
    }
  ],
  "year": 2005                      # field not specified by mapping, indexed anyway
}
```

# Searching

## Match all
Matches all documents.
```
GET /shed-talks/_search
{
  "query": {
    "match_all": {}
  }
}
```

## Term queries (exact matching)

### Term query
Exact match on field required.
```
GET /shed-talks/_search
{
  "query": {
    "term": {
      "title.keyword": "Militant atheism"   # exact match needed
    }
  }
}
```

### Terms queries
At least one of values in the array needs to be exact match.
```
GET /shed-talks/_search
{
  "query": {
    "terms": {
      "title.keyword": ["Militant atheism", "How to get (a new) hip"]
    }
  }
}
```

### Prefix queries
Prefix match on field.
```
GET /shed-talks/_search
{
  "query": {
    "prefix": {
      "title.keyword": "Militant"
    }
  }
}
```

### Wildcard query
Simplified regex matching.
```
GET /shed-talks/_search
{
  "query": {
    "wildcard": {
      "title.keyword": "Mil*nt *"
    }
  }
}

GET /shed-talks/_search
{
  "query": {
    "wildcard": {
      "title.keyword": "?ilitant atheism"
    }
  }
}
```
- '*' - matches multiple chars
- '?' - matches one char

### Regex query
Regular expression match.
```
GET /shed-talks/_search
{
  "query": {
    "regexp": {
      "title.keyword": "Mi[a-z]+nt atheism"
    }
  }
}
```

### Range query
Match range of values.
```
GET /shed-talks/_search
{
  "query": {
    "range": {
      "comments": {
        "gte": 1000,
        "lte": 3000
      }
    }
  }
}
```
- possible matches
    - gte, gt, lte, lt
- can be used for `dates` also

## Fulltext search

### Match query
Does fulltext searching on given field.
```
GET /shed-talks/_search
{
  "query": {
    "match": {
      "title": "car"
    }
  }
}
```

### Multi match query
Does fulltext searching on multiple fields.
```
GET /shed-talks/_search
{
  "query": {
    "multi_match": {
      "query": "car",
      "fields": ["title", "description"]
    }
  }
}
```

### Match phrase query
Words need to be on in a specified order.
```
GET /shed-talks/_search
{
  "query": {
    "match_phrase": {
      "title": "Militant atheism"
    }
  }
}
```

## Bool query
```
GET /shed-talks/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "title": "car"
          }
        }
      ],
      "should": [
         {
          "match": {
            "description": "robotics"
          }
        }
      ]
      "must_not": [
        {
          "match": {
            "description": "robotics"
          }
        }
      ],
      "filter": [
         {
          "range": {
            "comments": {
              "gte": 100
            }
          }
        }
      ]
    }
  }
}
```
- types
    - `must`
        - all queries must be matched in order for document to be considered as a match
        - matches contribute to scoring
    - `should`
        - match is not required, is optional
        - matches contribute to scoring
    - `must_not`
        - query must not match, if matches document is excluded
    - `filter`
        - all queries must be matched in order for document to be considered as a match
        - matches do NOT contribute to scoring
- queries can be arbitrarily nested
    - e.g.
    - ```
      "bool": {
        "must": [
            {
              "match": {
                "title": "car"
              }
            },
            {
              "bool": {
                "must": [
                    ...
                ]
              }
            }
      ]
      ```
